#pragma once

//Coordinates of a point, used to represent the bicks rectangle points;
class Coordinates {
public:
	float x;
	float y;

	float getX() {
		return x;
	}

	float getY() {
		return y;
	}

	void setX( float value) {
		x = value;
	}

	void setY(float value) {
		y = value;
	}
};