#pragma once

#include <Component/SimpleScene.h>
#include <string>
#include <Core/Engine.h>
#include "Brick.h"
#include "PowerUp.h"


#define BAR_Y 20.0f	//DISTANCE FROM BOTTOM TO BAR
#define ROWS 10		//NUMBER OF ROWS OF BRICKS;
#define COLS 12		//NUMEBER OF LINES OF BRICKS;
#define SPACE 23.0f	//SPACE BEWTEEN BRICKS;

//BALLSPEED
#define SPEED 350.0f	
#define FAST_SPEED 500.0f

//POWER UPS FREQUENCY ACCORDING TO THEI TYPE;
#define FREQ 8
#define BAR_FREQ 0
#define WALL_FREQ 1
#define SPEED_FREQ 2
#define LIFE_FREQ 3

constexpr auto CIRCLE_NO = 160;


class Tema1 : public SimpleScene
{
public:
	Tema1();
	~Tema1();

	void Init() override;
	void resolutionCalc();

private:
	void FrameStart() override;
	void Update(float deltaTimeSeconds) override;
	void FrameEnd() override;

	void OnInputUpdate(float deltaTime, int mods) override;
	void OnKeyPress(int key, int mods) override;
	void OnKeyRelease(int key, int mods) override;
	void OnMouseMove(int mouseX, int mouseY, int deltaX, int deltaY) override;
	void OnMouseBtnPress(int mouseX, int mouseY, int button, int mods) override;
	void OnMouseBtnRelease(int mouseX, int mouseY, int button, int mods) override;
	void OnMouseScroll(int mouseX, int mouseY, int offsetX, int offsetY) override;
	void OnWindowResize(int width, int height) override;

protected:
	void minusLife();
	void endLife();
	void calculateScaleFactor(float deltaTimeSeconds);
	void wallsCollision();
	void ballBarCollision();
	void ballBricksCollision(float deltaTimeSeconds);
	void powerUpCollision(float deltaTimeSeconds);
	bool newLevel();
	void newLevelSetup(int current_level, int current_lifes);
	glm::mat3 modelMatrix;

	Brick bricks[ROWS][COLS];
	std::vector <PowerUp> power;

	//Parameters used when scalling, calculating distance and timers;
	float barX;
	float wall_vert_width, wall_vert_length;
	float wall_oriz_width, wall_oriz_length;
	bool ballState;
	float pos_x_b, pos_y_b;
	float spaceSides;
	float spaceUp;
	float spaceDown;
	float brick_width, brick_height;
	int life_no;
	float window_height, window_width;
	float sign_y;
	float sign_x;
	int bricks_hit;
	int level_no;
	bool collision_bottom;
	float timer_bottom;
	float RADIUS;
	float RADIUS_2;
	float BAR_WIDTH;
	float BAR_LENGTH;
	float timer_bar;
	float bar_length_small;
	float bar_length_big;
	float bricks_per_time;
	float length;
	float timer_speed;
	float ball_speed;
	
};
