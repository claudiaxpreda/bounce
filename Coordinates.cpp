

class Coordinates {
	public:
		float x;
		float y;
	
		float get_X() {
			return x;
		}

		float get_Y() {
			return y;
		}
};