#include "PowerUp.h"
#include "Object2D.h"


PowerUp::PowerUp(){
	isAchieved = false;
	rotate_factor = 0;
}

PowerUp::~PowerUp(){}

void PowerUp::setLength(float value) {
	length = value;
}

float PowerUp::getLength() {
	return length;
}

void PowerUp::setType(int value) {
	powerType = value;
}

int PowerUp::getType() {
	return powerType;
}

void PowerUp::setAchieved(bool value) {
	isAchieved = true;
}
int PowerUp::typeAchieved() {

	if (getType() == S_LIFE)
		return S_LIFE;

	if (getType() == S_BAR)
		return S_BAR;
	if (getType() == S_WALL)
		return S_WALL;

	return S_SPEED;
}

void PowerUp::setRotateFactor(float value) {
	rotate_factor = value;
}
float PowerUp::getRotateFactor() {
	return rotate_factor;
}
float PowerUp::getCenterX() {
	
	return getLength() / 2.0f;
}

float PowerUp::getCenterY(){
	return  getLength() / 2.0f;
}

void PowerUp::setFalling(bool value) {
	falling = value;
}
bool PowerUp::getFalling() {
	return falling;
}


void PowerUp::setStartX(float value) {
	startX = value;
}
float PowerUp::getStartX() {
	return startX;
}
void PowerUp::setStartY(float value) {
	startY = value;
}
float PowerUp::getStartY() {
	return startY;
}

float PowerUp::getTimer() {
	return timer;
}
void PowerUp::setTimer(float value) {
	timer = value;
}

bool PowerUp::getAchieved() {
	return isAchieved;
}