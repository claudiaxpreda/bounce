#**Bounce Game**

###Description 
This is a little game designed using OpenGL, written in C++. 
A player is supposed to control a bouncing ball in order to break a brick wall.
At this moment the game has two levels. During the game some power ups are 
available.

####Power-ups( All power ups are available for a certain amount of time): 
1.	BLUE POWER UP : With this power up, a bottom wall appears;
2.	GREEN POWER UP: Maximize the bar;
3.	RED POWER UP: Bonus life;
4.	YELLOW POWER UP: Increase the ball speed ball; 

####Preview:
![Screenshot](https://bitbucket.org/claudiaxpreda/bounce/raw/master/photos/level1.PNG "Level1 Preview")

*	There are two more screenshots in the phothos/ folder;