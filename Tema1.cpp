#include "Tema1.h"

#include <vector>
#include <iostream>

#include <Core/Engine.h>
#include "Transform2D.h"
#include "Object2D.h"

using namespace std;

Tema1::Tema1()
{
}

Tema1::~Tema1()
{

}

//Function for window resize to keep proportion;
void Tema1::resolutionCalc() {

	glm::vec3 corner = glm::vec3(0, 0, 0);
	glm::ivec2 resolution = window->GetResolution();
	auto camera = GetSceneCamera();
	camera->SetOrthographic(0, (float)resolution.x, 0, (float)resolution.y, 0.01f, 400);
	camera->SetPosition(glm::vec3(0, 0, 50));
	camera->SetRotation(glm::vec3(0, 0, 0));
	camera->Update();
	GetCameraInput()->SetActive(false);

	//Window dimensions
	window_height = (float)resolution.x;
	window_width = (float)resolution.y;
	
	level_no = 1;
	//Bar position

	barX = window_height / 2.0f;
	pos_x_b = window_height / 2.0f - RADIUS / 2.0f;

	//Set ball parameters
	ball_speed = SPEED;
	RADIUS = 0.97f *window_height / 100.0f;
	RADIUS_2 = 1.1f *window_height / 100.0f;
	
	//Bar parameters;
	BAR_WIDTH  = 2.3f * window_width / 100.0f;
	BAR_LENGTH = 16.0f * window_height / 100.0f;
	bar_length_small = 16.0f * window_height / 100.0f;
	pos_y_b = 2.0f * BAR_Y + BAR_WIDTH + RADIUS;

	//Create game bar
	Mesh* bar = Object2D::CreateRectangle("bar", corner, bar_length_small, BAR_WIDTH, glm::vec3(0.5, 0.9, 0), true);
	AddMeshToList(bar);

	//Create the bigger bar for power ups
	bar_length_big = 30.0f * window_height / 100.0f;
	Mesh* big_bar = Object2D::CreateRectangle("barB", corner, bar_length_big, BAR_WIDTH, glm::vec3(0.5, 1, 0), true);
	AddMeshToList(big_bar);

	//Create the game ball;
	Mesh* game_ball = Object2D::CreateCircle("game_ball", corner, RADIUS, CIRCLE_NO, glm::vec3(1, 0, 0.3), true);
	AddMeshToList(game_ball);

	//Create the life ball;
	Mesh* life_ball = Object2D::CreateCircle("life_ball", corner, RADIUS_2, CIRCLE_NO, glm::vec3(0.51, 0.45, 0.89), true);
	AddMeshToList(life_ball);

	//Build the verticle and orizontal walls;
	wall_vert_width = 87.0f * window_width / 100.0f;
	wall_vert_length = 1.5f * window_height / 100.0f;
	Mesh* wall_vert = Object2D::CreateRectangle("wall_vert", corner, wall_vert_length, wall_vert_width, glm::vec3(0.86, 0.27, 0.09), true);
	AddMeshToList(wall_vert);

	wall_oriz_width = 1.5f * window_height / 100.0f;
	wall_oriz_length = window_height;
	Mesh* wall_length = Object2D::CreateRectangle("wall_oriz", corner, wall_oriz_length, wall_oriz_width, glm::vec3(0.86, 0.27, 0.09), true);
	AddMeshToList(wall_length);

	wall_oriz_width = 0.9f * window_height / 100.0f;
	wall_oriz_length = window_height;
	Mesh* wall_length_bottom = Object2D::CreateRectangle("wall_oriz_b", corner, wall_oriz_length, wall_oriz_width, glm::vec3(0.86, 0.27, 0.09), true);
	AddMeshToList(wall_length_bottom);

	//Calculate the dimension for a brick according to the space from the margins and the space between them;
	spaceSides = 15.0f * window_height / 100.0f;
	spaceUp = 7.0f * window_height / 100.f;
	spaceDown = 32.0f * window_width / 100.f;

	brick_width = (window_height - 2.0f * wall_vert_length - 2.0f*spaceSides - (COLS - 1)*SPACE) / COLS;
	brick_height = (window_width - spaceUp - spaceDown - wall_oriz_width - (ROWS - 1)*SPACE) / ROWS;

	//Creating bricks of different colors;
	Mesh* brick_1 = Object2D::CreateRectangle("brick_1", corner, brick_width, brick_height, glm::vec3(0.83, 0.36, 0.7), true);
	AddMeshToList(brick_1);

	Mesh* brick_2 = Object2D::CreateRectangle("brick_2", corner, brick_width, brick_height, glm::vec3(0.84, 0.36, 0.9), true);
	AddMeshToList(brick_2);

	Mesh* brick_3 = Object2D::CreateRectangle("brick_3", corner, brick_width, brick_height, glm::vec3(0.9, 0, 0.5), true);
	AddMeshToList(brick_3);

	// Create power ups types;
	length = 1.8f* window_height / 100.0f;

	//Bottom wall power up;
	Mesh* square_wall = Object2D::CreateSquare("square_w", corner, length, glm::vec3(0,0, 1), true);
	AddMeshToList(square_wall);

	//Bigger Bar power up;
	Mesh* square_bar = Object2D::CreateSquare("square_b", corner, length, glm::vec3(0, 1, 0), true);
	AddMeshToList(square_bar);

	//Falling life power up;
	Mesh* square_life = Object2D::CreateSquare("square_l", corner, length, glm::vec3(1, 0, 0), true);
	AddMeshToList(square_life);

	//Modify ball speed power up;
	Mesh* square_speed = Object2D::CreateSquare("square_sp", corner, length, glm::vec3(1, 1, 0), true);
	AddMeshToList(square_speed);

	//Create level index;
	Mesh* level = Object2D::CreateTriangle("level", corner, length, glm::vec3(0.45, 0.15, 0.76), true);
	AddMeshToList(level);

	//Calculate bricks position;
	float brick_pos_x;
	float brick_pos_y;
	for (int i = 0; i < ROWS; i++) {
		brick_pos_y = spaceDown + float(i) * (SPACE + brick_height);
		for (int j = 0; j < COLS; j++) {
			brick_pos_x = wall_vert_length + spaceSides + float(j) * (SPACE + brick_width);
			bricks[i][j].setCorners(brick_pos_x, brick_pos_y, brick_height, brick_width);
			if(((i + j) % 3 == 2 || (i + j) % 5 == 2) && (i+j) % 2 == 0 && level_no % 2 == 0)
				bricks[i][j].setHitNo(2);
			else 
				bricks[i][j].setHitNo(1);
		}
	}
}

//Method to reset the scene when a life is lost;
void Tema1::minusLife() {
	ballState = false;
	life_no = life_no - 1;
	pos_y_b = BAR_Y + BAR_WIDTH + 2 * RADIUS;
	pos_x_b = barX;
	sign_y = 1;
	sign_x = 0;
	ball_speed = SPEED;
	timer_bottom = 0;
	timer_speed = 0;
	timer_bar = 0;
	collision_bottom = false;
	power.clear();
}

//Checks if a level is finished;
bool Tema1::newLevel() {
	for (int i = 0; i < ROWS; i++)
		for (int j = 0; j < COLS; j++)
			if (bricks[i][j].notObstacle == false)
				return false;
	return true;
}

//Setting parameters at thei initial state;
void Tema1::endLife() {
	ballState = false;
	life_no = 3;
	BAR_LENGTH = bar_length_small;
	barX = window_height / 2.0f;
	pos_y_b = BAR_Y + BAR_WIDTH + 2 * RADIUS;
	pos_x_b = window_height / 2.0f - RADIUS / 2.0f;
	sign_y = 1;
	sign_x = 0;
	ball_speed = SPEED;
	pos_x_b = window_height / 2.0f - RADIUS / 2.0f;
	level_no = 1;
	for (int i = 0; i < ROWS; i++)
		for (int j = 0; j < COLS; j++) {
			bricks[i][j].setScaleFactor(1.0f);
			bricks[i][j].setHit(false);
			bricks[i][j].setHitNo(1);
		}
	bricks_hit = 0;
	timer_bottom = 0;
	timer_speed = 0;
	timer_bar = 0;
	collision_bottom = false;
	power.clear();
}

//Setting the new level parameters
void Tema1::newLevelSetup(int current_level, int current_lifes) {
		endLife();
		if (current_level == 1) {
			level_no = 2;
			for (int i = 0; i < ROWS; i++)
				for (int j = 0; j < COLS; j++) {
					if (((i + j) % 3 == 2 || (i + j) % 5 == 2) && (i + j) % 2 == 0)
						bricks[i][j].setHitNo(2);
				}
		}
		else
			level_no = 1;
		life_no = current_lifes;
}

//Calculate the scle factor for a hit brick;
void Tema1::calculateScaleFactor(float deltaTimeSeconds) {
	float scale;
	for (int i = 0; i < ROWS; i++)
		for (int j = 0; j < COLS; j++) {
			if (bricks[i][j].getScaleFactor() < 1) {
				scale = bricks[i][j].getScaleFactor();
				scale = scale - 1.8f * deltaTimeSeconds;
				if (scale <= 0) {
					scale = 0;
					bricks[i][j].setHit(true);
					bricks_hit += 1;
				}
				bricks[i][j].setScaleFactor(scale);
			}
		}
}

//Get the collision with the walls;
void Tema1::wallsCollision() {

	if (pos_y_b + RADIUS >= (float)window_width - wall_oriz_width) {
		sign_y = -1;
	}
	if (pos_x_b - RADIUS <= wall_vert_length && pos_y_b + RADIUS >= (float)window_width - wall_vert_width) {
		sign_x = 1;
	}
	if (pos_x_b + RADIUS >= (float)window_height - wall_vert_length && pos_y_b + RADIUS >= (float)window_width - wall_vert_width) {
		sign_x = -1;
	}
	if (collision_bottom == true) {
		if (pos_y_b <= wall_oriz_width && pos_x_b + RADIUS/2 >= 0 && pos_x_b - RADIUS/2 <= window_height) {
			sign_y = 1;
		}
	}
}

//Get the collision with the bar;
void Tema1::ballBarCollision() {
	float rad; 
	if (pos_y_b - RADIUS <= BAR_Y + BAR_WIDTH && pos_x_b >= barX - BAR_LENGTH / 2 - RADIUS && pos_x_b <= barX + BAR_LENGTH / 2 + RADIUS) {
		if (pos_x_b < barX) {
			rad = 2.0f *0.8f* (pos_x_b - barX) / BAR_LENGTH;
		}

		if (pos_x_b > barX) {
			rad = 2.0f *0.8f* (pos_x_b - barX) / BAR_LENGTH;
		}

		if (pos_x_b == barX) {
			rad = 0;
		}
		sign_y = sin(acos(rad));
		sign_x = cos(acos(rad));
	}
}

//Collect power ups;
void Tema1::powerUpCollision(float deltaTimeSeconds) {
	float time = 400.0f * deltaTimeSeconds;
	int type;
	for(int i = 0; i < power.size(); i++) {
		if (power.at(i).getFalling()) {
			float x = power.at(i).getStartX();
			float y = power.at(i).getStartY();
			type = power.at(i).typeAchieved();
			if (y >= 0 && y <= BAR_Y + BAR_WIDTH && x >= barX - BAR_LENGTH / 2 - RADIUS && x <= barX + BAR_LENGTH / 2 + RADIUS) {
				power.at(i).setAchieved(true);
				power.at(i).setFalling(false);
				
				if (type == S_WALL) {
					timer_bottom += time;
				}

				if (type == S_BAR) {
					timer_bar += time;
				}

				if (type == S_LIFE) {
					if (life_no < 3)
						life_no += 1;
				}
				if (type == S_SPEED) 
					timer_speed += time;
				
			}
		}
	}
}

//Calculate collisions with the bricks and setting parameters;
void Tema1::ballBricksCollision(float deltaTimeSeconds) {

	float scale;
	bricks_per_time = 0;

	for (int i = 0; i < ROWS; i++)
		for (int j = 0; j < COLS; j++) {

			if (!bricks[i][j].notObstacle) {

				if (pos_x_b >= bricks[i][j].getBottomLeftPointX() - RADIUS && pos_x_b <= bricks[i][j].getBottomRightPointX() + RADIUS
					&& pos_y_b + RADIUS >= bricks[i][j].getBottomLeftPointY() && pos_y_b - RADIUS <= bricks[i][j].getUpperLeftPointY()) {

					if (abs(pos_y_b + RADIUS - bricks[i][j].getBottomLeftPointY()) <= SPEED * deltaTimeSeconds ||
						abs(pos_y_b - RADIUS - bricks[i][j].getUpperLeftPointY()) <= SPEED * deltaTimeSeconds) {
							sign_y = -1 * sign_y;
					}


					if (abs(pos_x_b + RADIUS - bricks[i][j].getBottomLeftPointX()) <= SPEED * deltaTimeSeconds ||
						abs(pos_x_b - RADIUS - bricks[i][j].getBottomRightPointX()) <= SPEED * deltaTimeSeconds) {
							sign_x = -1 * sign_x;
					}

					int hit_no = bricks[i][j].getHitNo() - 1;
					bricks[i][j].setHitNo(hit_no);
					bricks_per_time += 1;
					
					if (bricks[i][j].getHitNo() == 0) {
						scale = bricks[i][j].getScaleFactor();
						scale = scale - 1.9f * deltaTimeSeconds;
						bricks[i][j].setScaleFactor(scale);
					}
					if(bricks_hit != 0) {
						if (bricks_hit % FREQ == 0) {
							PowerUp mypower;
							mypower.setLength(2.5f * window_height / 100.0f);
							mypower.setRotateFactor(deltaTimeSeconds);

							int option = rand() % (SPEED_FREQ + 3);
							//Setting type of power up quite random;
							if (option == WALL_FREQ)
								mypower.setType(S_WALL);
							if (option == BAR_FREQ)
								mypower.setType(S_BAR);
							if (option == LIFE_FREQ )
								mypower.setType(S_LIFE);
							if (option == S_SPEED)
								mypower.setType(S_SPEED);

							mypower.setFalling(true);
							mypower.setStartX(bricks[i][j].getUpperLeftPointX());
							mypower.setStartY(bricks[i][j].getUpperLeftPointY());
							power.push_back(mypower);
						}
					}
				}
			}
		}

}

//Initi function;
void Tema1::Init()
{
	resolutionCalc();
	glm::vec3 corner = glm::vec3(0, 0, 0);
	ballState = false;
	life_no = 3;
	sign_y = 1;
	bricks_hit = 0;
	collision_bottom = false;
	timer_bottom = 0;
	timer_bar = 0;
	
}

void Tema1::FrameStart()
{
	// clears the color buffer (using the previously set color) and depth buffer
	glClearColor(0, 0, 0, 1);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glm::ivec2 resolution = window->GetResolution();
	// sets the screen area where to draw
	glViewport(0, 0, resolution.x , resolution.y);
}

void Tema1::Update(float deltaTimeSeconds)
{	
	//Check no of lifes;
	if (life_no == 0) 
		endLife();

	//Check timer for speed power up;
	if (timer_speed > 0)
		timer_speed -= deltaTimeSeconds;
	else
		timer_speed = 0;

	//Output level;
	float x_lvl = BAR_Y;
	for (int i = 0; i < level_no; i++) {
		modelMatrix = glm::mat3(1);
		x_lvl += BAR_Y + length;
		modelMatrix *= Transform2D::Translate(window_height - x_lvl, window_width - length - wall_oriz_width - BAR_Y);
		RenderMesh2D(meshes["level"], shaders["VertexColor"], modelMatrix);
	}
	glm::ivec2 resolution = window->GetResolution();
	
	//Render the game ball;
	modelMatrix = glm::mat3(1);
	float mid_bar = BAR_LENGTH/ 2.0f;
	modelMatrix *= Transform2D::Translate(barX -mid_bar, BAR_Y);

	//Check timer for the bigger bar power up and output the corresponding bar;
	if (timer_bar > 0) {
		BAR_LENGTH = bar_length_big;
		RenderMesh2D(meshes["barB"], shaders["VertexColor"], modelMatrix);
		timer_bar -= deltaTimeSeconds;
	}
	else {
		timer_bar = 0;
		BAR_LENGTH = bar_length_small;
		RenderMesh2D(meshes["bar"], shaders["VertexColor"], modelMatrix);
	}

	//Reneder the vertical wall left;
	modelMatrix = glm::mat3(1);
	float pos_x = 0.0;
	float pos_y = (float)resolution.y - wall_vert_width;
	modelMatrix *= Transform2D::Translate(pos_x, pos_y);
	RenderMesh2D(meshes["wall_vert"], shaders["VertexColor"], modelMatrix);

	//Render the vertical wall right;
	modelMatrix = glm::mat3(1);
	pos_x = (float)resolution.x - wall_vert_length;
    pos_y = (float)resolution.y - wall_vert_width;
	modelMatrix *= Transform2D::Translate(pos_x, pos_y);
	RenderMesh2D(meshes["wall_vert"], shaders["VertexColor"], modelMatrix);
	
	//Render the orizontal wall;
	modelMatrix = glm::mat3(1);
	pos_x = 0.0;
	pos_y = (float)resolution.y - wall_oriz_width;
	modelMatrix *= Transform2D::Translate(pos_x, pos_y);
	RenderMesh2D(meshes["wall_oriz"], shaders["VertexColor"], modelMatrix);
	
	//Calculating the scale factor for the hit bricks;
	calculateScaleFactor(deltaTimeSeconds);

	//Changing the ball movement based on its collision with the obstacles;
	modelMatrix = glm::mat3(1);
	if (ballState) {

		wallsCollision();
		ballBarCollision();
		ballBricksCollision(deltaTimeSeconds);
		
		//Ball outside the window, substract one life based on the power up for bottom wall existence;
		if (collision_bottom == false) {
			if (pos_y_b - RADIUS < BAR_Y + BAR_WIDTH && (pos_x_b <+barX - BAR_LENGTH / 2 - RADIUS || pos_x_b >+barX + BAR_LENGTH / 2 + RADIUS))
				minusLife();
			if (pos_y_b < wall_vert_width && (pos_x_b < 0 - RADIUS - BAR_LENGTH / 2 || pos_x_b >window_height + BAR_LENGTH / 2 + RADIUS)) {
				minusLife();
			}
		}
		else {
			if (pos_y_b < wall_vert_width &&pos_y_b >wall_oriz_width && (pos_x_b < 0 - RADIUS - BAR_LENGTH / 2 || pos_x_b >(float)resolution.x + BAR_LENGTH / 2 + RADIUS)) 
				minusLife();
		}
		pos_y_b += sign_y * ball_speed* deltaTimeSeconds;
		pos_x_b += sign_x * ball_speed * deltaTimeSeconds;
	}
	
	modelMatrix = glm::mat3(1);
	modelMatrix *= Transform2D::Translate(pos_x_b, pos_y_b);
	RenderMesh2D(meshes["game_ball"], shaders["VertexColor"], modelMatrix);

	//Render the lifes ball;
	float pos_x_bb = 0;
	float pos_y_bb;
	for (int i = 0; i < life_no; i++) {
		modelMatrix = glm::mat3(1);
		pos_x_bb += 10.f + 2 * RADIUS_2;
	    pos_y_bb = 2.0f * BAR_Y;
		modelMatrix *= Transform2D::Translate(pos_x_bb, pos_y_bb);
		RenderMesh2D(meshes["life_ball"], shaders["VertexColor"], modelMatrix);
	}

	//Render the bricks
	for (int i = 0; i < ROWS; i++) {
		for (int j = 0; j < COLS; j++) {
			modelMatrix = glm::mat3(1);
			modelMatrix *= Transform2D::Translate(bricks[i][j].getBottomLeftPointX(), bricks[i][j].getBottomLeftPointY());
			modelMatrix *= Transform2D::Scale(bricks[i][j].getScaleFactor(), bricks[i][j].getScaleFactor());
			if ((i + j) % 2 == 1)
				RenderMesh2D(meshes["brick_2"], shaders["VertexColor"], modelMatrix);
			else {
				if ( ((i + j) % 3 == 2 || (i+j)% 5 == 2) && level_no % 2 == 0) {
					RenderMesh2D(meshes["brick_3"], shaders["VertexColor"], modelMatrix);
				}
				else
					RenderMesh2D(meshes["brick_1"], shaders["VertexColor"], modelMatrix);

			}
		}
	}

	//Collecting power ups;
	powerUpCollision(deltaTimeSeconds);

	//Managing power ups;
	for (int i = 0; i < power.size(); i++) {
		//printf("Here\n");
		if (power.at(i).getFalling()) {
			modelMatrix = glm::mat3(1);
			modelMatrix *= Transform2D::Translate(power.at(i).getStartX(), power.at(i).getStartY());
			modelMatrix *= Transform2D::Translate(power.at(i).getCenterX(), power.at(i).getCenterY());
			modelMatrix *= Transform2D::Rotate(power.at(i).getRotateFactor());
			modelMatrix *= Transform2D::Translate(-power.at(i).getCenterX(), -power.at(i).getCenterY());
			if (power.at(i).getType() == S_WALL)
				RenderMesh2D(meshes["square_w"], shaders["VertexColor"], modelMatrix);
			if (power.at(i).getType() == S_BAR)
				RenderMesh2D(meshes["square_b"], shaders["VertexColor"], modelMatrix);
			if (power.at(i).getType() == S_LIFE)
				RenderMesh2D(meshes["square_l"], shaders["VertexColor"], modelMatrix);
			if (power.at(i).getType() == S_SPEED)
				RenderMesh2D(meshes["square_sp"], shaders["VertexColor"], modelMatrix);
			float x = power.at(i).getRotateFactor();
			x += deltaTimeSeconds;
			float y = power.at(i).getStartY() - SPEED * deltaTimeSeconds / 1.1f;
			power.at(i).setStartY(y);
			power.at(i).setRotateFactor(x);
			float time = power.at(i).getTimer();
			time -= deltaTimeSeconds;
			power.at(i).setTimer(time);
		}
	}
		
	if (timer_bottom > 0) {
			collision_bottom = true;
			modelMatrix = glm::mat3(1);
			float x = 0.0f;
			float y = 0.0f;
			modelMatrix *= Transform2D::Translate(x, y);
			RenderMesh2D(meshes["wall_oriz_b"], shaders["VertexColor"], modelMatrix);
			timer_bottom -= deltaTimeSeconds;
	}

	//Check timer for bottom wall power up;
	if (timer_bottom < 0) {
		collision_bottom = false;
		timer_bottom = 0;
	}
	
	//Check if switching to a new level;
	if (newLevel())
		newLevelSetup(level_no, life_no);
}

void Tema1::FrameEnd()
{

}

void Tema1::OnInputUpdate(float deltaTime, int mods)
{
}

void Tema1::OnKeyPress(int key, int mods)
{
	//Switch speed when corresponding power up;
	if (key == GLFW_KEY_A) {
		if (timer_speed > 0) {
			if (ball_speed == SPEED)
				ball_speed = FAST_SPEED;
			else
				ball_speed = SPEED;
		}
	}

	//Switch level;
	if (key == GLFW_KEY_1) {
		newLevelSetup(1, 3);
		ballState = false;
	}

	if (key == GLFW_KEY_2) {
		newLevelSetup(2, 3);
		ballState = false;
	}

	//Exit();
	if (key == GLFW_KEY_E) {
		exit(-1);

	}

}

void Tema1::OnKeyRelease(int key, int mods)
{
	// add key release event
}

void Tema1::OnMouseMove(int mouseX, int mouseY, int deltaX, int deltaY)
{	
	//BarMovement;
	barX = (float)mouseX;
	
	if (!ballState) {
		pos_x_b = (float) mouseX;
	}
}

void Tema1::OnMouseBtnPress(int mouseX, int mouseY, int button, int mods)
{
	ballState = true;

		
}

void Tema1::OnMouseBtnRelease(int mouseX, int mouseY, int button, int mods)
{
	// add mouse button release event
}

void Tema1::OnMouseScroll(int mouseX, int mouseY, int offsetX, int offsetY)
{
}

void Tema1::OnWindowResize(int width, int height)
{	
	resolutionCalc();
}
