#pragma once
#include "Coordinates.h"

//Collision types;
#define BOTTOM 0
#define UP	1
#define LEFT 2
#define RIGHT 3
#define NO_COLLISION -1

//Pattern of bricks which are represented by rectangle;
//Name of methods are suggestive, setting parameters and returning them;
class Brick
{
public:
	Brick();
	~Brick();

	float getBottomLeftPointX();
	float getBottomLeftPointY();
	float getBottomRightPointX();
	float getBottomRightPointY();
	float getUpperLeftPointX();
	float getUpperLeftPointY();
	float getUpperRightPointX();
	float getUpperRightPointY();
	void setBottomLeftPointX(float value);
	void setBottomLeftPointY(float value);
	void setBottomRightPointX(float value);
	void setBottomRightPointY(float value);
	void setUpperLeftPointX(float value);
	void setUpperLeftPointY(float value);
	void setUpperRightPointX(float value);
	void setUpperRightPointY(float value);
	void setCorners(float x, float y, float height, float width);
	void setHit(bool value);
	void setHitNo(int value);
	int getHitNo();
	void setScaleFactor(float value);
	float getScaleFactor();
	float getCenterY();
	float getCenterX();

	//The four points of a rectangle;
	Coordinates  bottomLeft;
	Coordinates	 bottomRight;
	Coordinates	 upperLeft;
	Coordinates  upperRight;
	
	bool notObstacle;
	int hits_no;	//number of hits needed to break a brick;
	float scale_factor;	//the scaling factor when a brick is hit;

};
