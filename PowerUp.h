#pragma once
#include <string>

#include <include/glm.h>
#include <Core/GPU/Mesh.h>

//Types of power ups;
#define S_LIFE	1	//One life;
#define S_BAR	2	// Bigger bar;
#define S_WALL	3	//Bottom Wall
#define S_SPEED 4   //Ball Speed

//Contains a pattern for power ups
//Methos name suggestive, setting and returning parameters;

class PowerUp
{
public:
	PowerUp();
	~PowerUp();

	void setLength(float value);
    float getLength();
	
	void setType(int value);
	int  getType();

	void setAchieved(bool value);
	bool getAchieved();

	int  typeAchieved();

	void setRotateFactor(float value);
	float getRotateFactor();

	float  getCenterX();
	float  getCenterY();

	void setFalling(bool value);
	bool getFalling();

	void setStartX(float value);
	void setStartY(float value);

	float getStartX();
	float getStartY();

	float getTimer();
	void setTimer(float value);

	float length;	//power up side, is a square;
	bool isAchieved;	//Was caught;
	float rotate_factor;
	int powerType;
	bool falling;	//Is falling down;
	float startX, startY;
	float timer;

};
