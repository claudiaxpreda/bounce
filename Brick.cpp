#include "Brick.h"


Brick::Brick()
{
	//initialize parameters;
	notObstacle = false;
	scale_factor = 1;
}

Brick::~Brick()
{
}

float Brick::getBottomLeftPointX() {
	return bottomLeft.getX();
}
float Brick:: getBottomLeftPointY() {
	return bottomLeft.getY();
}
float Brick::getBottomRightPointX() {
	return bottomRight.getX();
}
float Brick::getBottomRightPointY() {
	return bottomRight.getY();
}
float Brick::getUpperLeftPointX() {
	return upperLeft.getX();
}

float Brick::getUpperLeftPointY() {
	return upperLeft.getY();
}

float Brick::getUpperRightPointX() {
	return upperRight.getX();
}

float Brick::getUpperRightPointY() {
	return upperRight.getY();
}

void Brick::setBottomLeftPointX(float value) {
	bottomLeft.setX(value);
}
void Brick::setBottomLeftPointY(float value) {
	bottomLeft.setY(value);
}
void Brick::setBottomRightPointX(float value) {
	bottomRight.setX(value);
}
void Brick::setBottomRightPointY(float value) {
	bottomRight.setY(value);
}
void Brick::setUpperLeftPointX(float value) {
	upperLeft.setX(value);
}
void Brick::setUpperLeftPointY(float value) {
	upperLeft.setY(value);
}
void Brick::setUpperRightPointX(float value) {
	upperRight.setX(value);
}
void Brick::setUpperRightPointY(float value) {
	upperRight.setY(value);
}

float Brick::getCenterX() {
	return getBottomRightPointX() / 2;
}

float Brick::getCenterY() {
	return getUpperLeftPointY() / 2;
}

void Brick::setScaleFactor(float value) {
	scale_factor = value;
}

float Brick::getScaleFactor() {
	return scale_factor;
}
void Brick::setHit(bool value) {
	notObstacle = value;
}

void Brick::setHitNo(int value) {
	hits_no = value;
}
int Brick::getHitNo() {
	return hits_no;
}

void Brick::setCorners(float x, float y, float height, float width) {

	Brick::setBottomLeftPointX(x);
	Brick::setBottomLeftPointY(y);
	Brick::setBottomRightPointX(x + width);
	Brick::setBottomRightPointY(y);
	Brick::setUpperLeftPointX(x);
	Brick::setUpperLeftPointY(y + height);
	Brick::setUpperRightPointX(x + width);
	Brick::setUpperRightPointY(y + height);
}
